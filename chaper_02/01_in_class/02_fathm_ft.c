#include <stdio.h>
int main(void)
{
	int feet, fathoms;		/* 声明两个整型变量feet（英尺，英制长度测量单位）和fathoms（英寻，英制深度测量单位） */

	fathoms = 2;			/* 为fathoms赋值为2 */
	feet = 6 * fathoms;		/* 定义fathoms和feet之间的换算关系 */
	printf("There are %d feet in %d fathoms!\n", feet, fathoms);	/* 按需打印feet和fathoms，并换行 */
	printf("Yes, I said %d feet!\n", 6 * fathoms);			/* 按需打印feet，并以fathoms换算关系式格式化输出 */


	return 0;
}
