#include <stdio.h>
/* 
	函数原型(prototype)；亦称，函数声明(function declaration)；告知编译器在程序中要使用该函数 
	其中前面的void，用来告知编译器该函数没有返回值，butler(void)中的void则代表该函数没有定义参数
*/
void butler(void);		



int main(void)
{
	printf("I will summon the butler function.\n");		
	butler();			/* 函数调用(function call)，告知编译器在此处调用该函数 */
	printf("Yes. Bring me some tea and writeable DVDs.\n");

	return 0;
}

void butler(void)		/* 函数定义(function definition)，告知编译器该函数的内容 */
{

	printf("You rang, sir?\n");

}
