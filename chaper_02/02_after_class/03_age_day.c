/*
    3. 编写一个程序把你的年龄转换成天数，并显示这两个值。这里不用考虑闰年的问题。
*/

#include <stdio.h>
int main(void)
{
    int age, day;
    printf("请输入你的年龄：");
    scanf("%d", &age);
    day = age * 365;
    printf("你的年龄是%d岁，你到目前为止已存活%d天。\n", age, day);

    return 0;
}