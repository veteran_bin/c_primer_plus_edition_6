/*
    1. 编写一个程序，调用一次printf()函数，把你的名和姓打印在一行。再调用一次printf()函数，把你的名和姓分别答应在两行。
       然后，在调用两次printf()函数，把你的名和姓打印在一行。输出应如下所示(当然要把示例的内容换成你的名字)：
       Gustav Mahler    
       Gustav           
       Mahler           
       Gustav Mahler    
*/

#include <stdio.h>
int main(void)
{
    printf("Gustav Mahler\n");
    printf("Gustav\n");
    printf("Mahler\n");
    printf("Gustav Mahler\n");

    return 0;
}