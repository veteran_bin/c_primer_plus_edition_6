/*
    6. 编写一个程序，创建一个整型变量toes，并将toes设置为10。程序中还要计算toes的两倍和toes的平方。
       该程序打印3个值，并分别描述以示区分。
*/

#include <stdio.h>
int main(void)
{
    int toes;
    toes = 10;
    printf("toes的值为：%d\n", toes);
    printf("toes两倍的值为：%d\n", toes * 2);
    printf("toes平方的值为：%d\n", toes * toes);

    return 0;
}