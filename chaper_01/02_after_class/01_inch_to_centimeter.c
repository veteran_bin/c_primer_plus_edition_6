/*
你刚被MacroMuscle有限公司聘用。该公司准备进入欧洲市场，需要一把英寸单位转换为厘米单位（1英寸 = 2.54厘米）的程序。该程序要提示用户输入英寸值。你的任务是定义程序目标和设计程序（编程工程的第1步和第2步）。
*/

#include <stdio.h>
int main(void)
{
	printf("请输入英寸值：");	/* 提示用户要输入的信息  */
	float inch;		      /* 声明一个float变量inch，用来存储用户输入的英寸值  */
	float centimeter;             /* 声明一个float变量centimeter，用来存储换算后得到的厘米值 */
	scanf("%f", &inch);	      /* 使用scanf()函数来接收用户输入的值，并将其赋值给float类型变量inch */
	centimeter = 2.54 * inch;     /* 基于换算公式，将inch换算成centimeter */
	printf("您输入的英寸值经过换算后，约等于%f厘米。\n", centimeter);    /* 打印换算之后的结果 */




	return 0;
}
